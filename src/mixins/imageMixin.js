export default {
    methods: {
        addImage() {
            this.form.newImages.push( { data: null, base64: '', preview: '' } );
        },
        createImage(index) {
            if (this.form.newImages.length === 0
                || !this.form.newImages[index]
                || !this.form.newImages[index].data
            ) {
                return;
            }

            const reader = new FileReader();
            reader.onload = (e) => {
                this.form.newImages[index].base64 = e.target.result;
            };
            reader.readAsDataURL(this.form.newImages[index].data);
            this.form.newImages[index].preview = URL.createObjectURL(this.form.newImages[index].data);
        },
        deleteImage(index) {
            if (this.form.newImages.length === 0 || !this.form.newImages[index]) {
                return;
            }
            this.form.newImages.splice(index, 1);
        },
        validateImage(item, index) {
            const errors = [];
            if (this.errors.newImages
                && this.errors.newImages.length > 0
                && this.errors.newImages[index]
            ) {
                errors.push(this.$t(this.errors.newImages[index][0]));
                this.errors.newImages[index] = null;
            }
            if (!item.$dirty) return errors;
            !item.isFileSize && errors.push(
                this.$t('validation.imageSizeMustBeLessThan', { num: '5' })
            );
            return errors;
        },
        removeEmptyImages() {
            this.form.newImages = this.form.newImages.filter(image => {
                return image.base64 && image.base64 !== '' && image.base64 !== undefined
            });
        },
        addToDeletedImageList(index) {
            if (this.form.storedImages.length === 0 || !this.form.storedImages[index]) {
                return;
            }
            let image = this.form.storedImages[index];
            this.form.deletedImages.push( { id: image.id, path: image.path, name: image.name } );
            this.form.storedImages.splice(index, 1);
        },
        addToActiveImageList(index) {
            if (this.form.deletedImages.length === 0 || !this.form.deletedImages[index]) {
                return;
            }
            let image = this.form.deletedImages[index];
            this.form.storedImages.push( { id: image.id, path: image.path, name: image.name } );
            this.form.deletedImages.splice(index, 1);
        },
    }
}