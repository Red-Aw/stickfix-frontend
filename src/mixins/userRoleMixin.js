export default {
    computed: {
        isSuperAdmin() {
            if (Array.isArray(this.roles)) {
                return this.roles.some(record => record.role === 'superAdmin');
            }
            return false;
        },
        getVisibleRoles() {
            if (Array.isArray(this.allRoles) && this.allRoles.length) {
                return this.allRoles.filter(item =>
                    this.isSuperAdmin
                        || (item !== 'superAdmin'
                            && item !== 'editUser'
                            && item !== 'markUser'
                            && item !== 'enterAdminCp')
                );
            } else {
                return [];
            }
        },
    },
    methods: {
        getIsActionVisible(action) {
            if (this.roles) {
                return this.isSuperAdmin || !action.role || this.userHasRole(action.role);
            } else {
                return false;
            }
        },
        getIsActionAllowed(role) {
            if (this.roles) {
                return this.isSuperAdmin || this.userHasRole(role);
            } else {
                return false;
            }
        },
        userHasRole(role) {
            if (Array.isArray(this.roles)) {
                return this.roles.some(record => record.role === role);
            }
            return false;
        },
    }
}