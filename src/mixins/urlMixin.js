export default {
    methods: {
        getCorrectUrl() {
            return process.env.VUE_APP_IMAGE_URL + '/';
        },
        getPreUrl() {
            let preUrl = '';
            Object.keys(this.$route.params).forEach(() => {
                preUrl += '../';
            });
            return preUrl;
        },
    }
}