export default {
    methods: {
        getSizes() {
            if (this.form.category === 'HELMET') {
                return this.sizesEnum.HELMET;
            } else if (this.form.category === 'GLOVES') {
                return this.sizesEnum.GLOVES;
            } else if (this.form.category === 'PANTS'
                || this.form.category === 'SHOULDER_PADS'
                || this.form.category === 'ELBOW_PADS'
            ) {
                return this.sizesEnum.PANTS_SHOULDER_ELBOW_PAD_SIZES;
            } else if (this.form.category === 'SHIN_GUARDS') {
                return this.sizesEnum.SHIN_GUARD;
            } else if (this.form.category === 'BAGS') {
                return this.sizesEnum.BAG;
            } else if (this.form.category === 'KITS') {
                return this.sizesEnum.KIT;
            } else {
                return [];
            }
        },
        getSkateSizes() {
            const skateSizes = [ ];
            Object.entries(this.sizesEnum.SKATES.LENGTH).forEach(entry => {
                const [key, value] = entry;
                skateSizes.push( key + ' - EUR ' + value.EUR );
            });

            return skateSizes;
        },
        resetSize() {
            this.form.size = '';
        },
    }
}