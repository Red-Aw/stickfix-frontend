export default {
    methods: {
        getSelectionItems(items) {
            let selectionList = [];

            items.forEach(item => {
                selectionList.push({
                    text: this.$t('enums.' + item),
                    value: item,
                });
            });

            return selectionList;
        },
        getSelectionItemsWithoutEnumValues(items) {
            let selectionList = [];

            items.forEach(item => {
                selectionList.push({
                    text: item,
                    value: item,
                });
            });

            return selectionList;
        },
        prepareExistingList(items) {
            items.forEach((item) => {
                item.text = this.$t('enums.' + item.value);
            });

            return items;
        },
    }
}