export default {
    methods: {
        getEntries(object) {
            return Object.entries(object);
        },
    }
}