export default {
    methods: {
        resetMessageTimeout(time) {
            setTimeout(() => {
                this.errors.visible = false;
            }, time)
        },
        setMessage(messageText) {
            this.loading = true;
            this.setMessageAction({
                message: messageText,
                isVisible: true,
                type: 'error'
            });
        },
    }
}