import Vue from 'vue'
import App from './App';
import axios from './axios.js';
Vue.prototype.$http = axios;

import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css';
import router from './router';

import Vuetify from 'vuetify';
Vue.use(Vuetify);
let vuetify = new Vuetify({
    theme: { dark: true },
})

import VueTelInputVuetify from 'vue-tel-input-vuetify/lib';
Vue.use(VueTelInputVuetify, { vuetify });

import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

import { VueReCaptcha } from 'vue-recaptcha-v3';
Vue.use(VueReCaptcha, {
    siteKey: process.env.VUE_APP_RECAPTCHA_SITE_KEY,
    loaderOptions: {
        autoHideBadge: true,
    }
});

import store from './store/vuex';

import './filters.js';
import i18n from './locales/localization';

import VueCookies from 'vue-cookies';
Vue.use(VueCookies);

store.dispatch('items/getItemsList');
store.dispatch('services/getServicesList');
store.dispatch('offers/getOffersList');
store.dispatch('enums/getAllEnums');
store.dispatch('message/setData');
store.dispatch('messages/getMessagesList');
store.dispatch('instagram/getInstagramFeed');
store.dispatch('auth/me').then(() => {
    new Vue({
        router,
        vuetify,
        render: h => h(App),
        store,
        i18n,
    }).$mount('#app');
}).catch((error) => {
    console.log('Error creating app instance', error);
});
