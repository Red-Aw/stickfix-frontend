import axios from './../axios.js';

export default {
    namespaced: true,

    state: {
        posts: null,
        isLoading: true,
    },

    getters: {
        posts (state) {
            return state.posts
        },
        isLoading (state) {
            return state.isLoading
        },
    },

    mutations: {
        SET_POSTS (state, value) {
            state.posts = value
        },
        SET_IS_LOADING (state, value) {
            state.isLoading = value
        },
    },

    actions: {
        getInstagramFeed ({ commit }) {
            commit('SET_IS_LOADING', true);
            return axios.get('/getInstagramFeed').then(response => {
                if (response.data.success) {
                    commit('SET_POSTS', response.data.posts);
                    commit('SET_IS_LOADING', false);
                } else {
                    commit('SET_POSTS', []);
                    commit('SET_IS_LOADING', true);
                }
            }).catch((error) => {
                console.log(error);
                commit('SET_POSTS', []);
                commit('SET_IS_LOADING', true);
            });
        }
    }
}
