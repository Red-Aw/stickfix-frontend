import axios from './../axios.js';

export default {
    namespaced: true,

    state: {
        itemsList: null,
        isLoading: true,
    },

    getters: {
        itemsList (state) {
            return state.itemsList
        },
        isLoading (state) {
            return state.isLoading
        },
    },

    mutations: {
        SET_ITEMS_LIST (state, value) {
            state.itemsList = value
        },
        SET_IS_LOADING (state, value) {
            state.isLoading = value
        },
    },

    actions: {
        async getItems ({ dispatch }) {
            return dispatch('getItemsList')
        },

        getItemsList ({ commit }) {
            commit('SET_IS_LOADING', true);
            return axios.get('/getItems').then(response => {
                if (response.data.success) {
                    commit('SET_ITEMS_LIST', response.data.data.items);
                    commit('SET_IS_LOADING', false);
                } else {
                    commit('SET_ITEMS_LIST', []);
                    commit('SET_IS_LOADING', true);
                }
            }).catch((error) => {
                console.log(error);
                commit('SET_ITEMS_LIST', []);
                commit('SET_IS_LOADING', true);
            });
        }
    }
}
