import axios from './../axios.js';

export default {
    namespaced: true,

    state: {
        categories: [],
        states: [],
        messageOptions: [],
        serviceTypes: [],
        brands: [],
        sizes: [],
        languages: [],
    },

    getters: {
        categories (state) {
            return state.categories;
        },
        states (state) {
            return state.states;
        },
        messageOptions (state) {
            return state.messageOptions;
        },
        serviceTypes (state) {
            return state.serviceTypes;
        },
        brands (state) {
            return state.brands;
        },
        sizes (state) {
            return state.sizes;
        },
        languages (state) {
            return state.languages;
        },
    },

    mutations: {
        SET_CATEGORIES (state, value) {
            state.categories = value
        },
        SET_STATES (state, value) {
            state.states = value
        },
        SET_MESSAGE_OPTIONS (state, value) {
            state.messageOptions = value
        },
        SET_SERVICE_TYPES (state, value) {
            state.serviceTypes = value
        },
        SET_BRANDS (state, value) {
            state.brands = value
        },
        SET_SIZES (state, value) {
            state.sizes = value
        },
        SET_LANGUAGES (state, value) {
            state.languages = value
        },
    },

    actions: {
        getAllEnums({ commit }) {
            return axios.get('/getAllEnums').then(response => {
                commit('SET_CATEGORIES', response.data.data.CATEGORIES);
                commit('SET_STATES', response.data.data.STATES);
                commit('SET_MESSAGE_OPTIONS', response.data.data.MESSAGE_OPTIONS);
                commit('SET_SERVICE_TYPES', response.data.data.SERVICE_TYPES);
                commit('SET_BRANDS', response.data.data.BRANDS);
                commit('SET_SIZES', response.data.data.SIZES);
                commit('SET_LANGUAGES', response.data.data.LANGUAGES);
            }).catch((error) => {
                console.log(error);
                commit('SET_CATEGORIES', []);
                commit('SET_STATES', []);
                commit('SET_MESSAGE_OPTIONS', []);
                commit('SET_SERVICE_TYPES', []);
                commit('SET_BRANDS', []);
                commit('SET_SIZES', []);
                commit('SET_LANGUAGES', []);
            });
        },

        getCategories({ commit }) {
            return axios.get('/getCategories').then(response => {
                commit('SET_CATEGORIES', response.data.data.CATEGORIES);
            }).catch((error) => {
                console.log(error);
                commit('SET_CATEGORIES', []);
            });
        },

        getStates({ commit }) {
            return axios.get('/getStates').then(response => {
                commit('SET_STATES', response.data.data.STATES);
            }).catch((error) => {
                console.log(error);
                commit('SET_STATES', []);
            });
        },

        getMessageOptions({ commit }) {
            return axios.get('/getMessageOptions').then(response => {
                commit('SET_MESSAGE_OPTIONS', response.data.data.MESSAGE_OPTIONS);
            }).catch((error) => {
                console.log(error);
                commit('SET_MESSAGE_OPTIONS', []);
            });
        },

        getServiceTypes({ commit }) {
            return axios.get('/getServiceTypes').then(response => {
                commit('SET_SERVICE_TYPES', response.data.data.SERVICE_TYPES);
            }).catch((error) => {
                console.log(error);
                commit('SET_SERVICE_TYPES', []);
            });
        },

        getBrands({ commit }) {
            return axios.get('/getBrands').then(response => {
                commit('SET_BRANDS', response.data.data.BRANDS);
            }).catch((error) => {
                console.log(error);
                commit('SET_BRANDS', []);
            });
        },

        getLanguages({ commit }) {
            return axios.get('/getLanguages').then(response => {
                commit('SET_LANGUAGES', response.data.data.LANGUAGES);
            }).catch((error) => {
                console.log(error);
                commit('SET_LANGUAGES', []);
            });
        },
    }
}
