import axios from 'axios';

export default axios.create({
    baseURL: process.env.NODE_ENV === 'production'
        ? '/api'
        : process.env.VUE_APP_BE_URL + '/api',
    withCredentials: true,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
    },
    transformRequest: [function (data, headers) {
        headers['Authorization'] = 'Bearer ' + getCookie('token')
        return data;
    }, ...axios.defaults.transformRequest],
});

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }

    return null;
}