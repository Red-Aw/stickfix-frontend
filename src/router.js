import Vue from 'vue';
import VueRouter from 'vue-router';

import HomePage from './pages/HomePage';
import AdvertsPage from './pages/AdvertsPage';
import NotFoundPage from './pages/NotFoundPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import EditUserPage from './pages/EditUserPage';
import AddItemPage from './pages/AddItemPage';
import EditItemPage from './pages/EditItemPage';
import AddServicePage from './pages/AddServicePage';
import EditServicePage from './pages/EditServicePage';
import AddOfferPage from './pages/AddOfferPage';
import EditOfferPage from './pages/EditOfferPage';
import AccountPage from './pages/AccountPage';
import NewPassPage from './pages/NewPassPage';
import ChangeLanguagePage from './pages/ChangeLanguagePage';
import AdminControlPanelPage from './pages/AdminControlPanelPage';
import MessagesPage from './pages/MessagesPage';
import ViewMessagePage from './pages/ViewMessagePage';

import axios from './axios.js';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '*',
            component: NotFoundPage,
            name: 'NotFoundPage',
            allowFullScreen: true
        },
        {
            path: '/',
            component: HomePage,
            name: 'HomePage'
        },
        {
            path: '/adverts',
            component: AdvertsPage,
            name: 'AdvertsPage',
        },
        {
            path: '/login',
            component: LoginPage,
            name: 'LoginPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (!response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/register',
            component: RegisterPage,
            name: 'RegisterPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth/addNewUser').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/editUser/:id',
            component: EditUserPage,
            name: 'EditUserPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth/editUser').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/newItem',
            component: AddItemPage,
            name: 'AddItemPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/editItem/:id',
            component: EditItemPage,
            name: 'EditItemPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/newService',
            component: AddServicePage,
            name: 'AddServicePage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/editService/:id',
            component: EditServicePage,
            name: 'EditServicePage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/newOffer',
            component: AddOfferPage,
            name: 'AddOfferPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/editOffer/:id',
            component: EditOfferPage,
            name: 'EditOfferPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/account',
            component: AccountPage,
            name: 'AccountPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/newPass',
            component: NewPassPage,
            name: 'NewPassPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/changeLanguage',
            component: ChangeLanguagePage,
            name: 'ChangeLanguagePage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/acp',
            component: AdminControlPanelPage,
            name: 'AdminControlPanelPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth/enterAdminCp').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/messages',
            component: MessagesPage,
            name: 'MessagesPage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth/readMessages').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
        {
            path: '/viewMessage/:id',
            component: ViewMessagePage,
            name: 'ViewMessagePage',
            beforeEnter: (to, from, next) => {
                axios.get('/checkAuth/replyOnMessages').then(response => {
                    if (response.data.success) {
                        return next();
                    }
                    return next({ name: 'NotFoundPage'})
                }).catch(() => {
                    return next({ name: 'NotFoundPage'})
                });
            },
        },
    ],
    mode: 'history',
    linkActiveClass: 'active',
    scrollBehavior() {
        return { top: 0 };
    },
});