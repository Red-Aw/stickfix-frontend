import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';
import VueCookies from 'vue-cookies';
import VueI18n from 'vue-i18n';

import MessagesPage from '@/pages/MessagesPage.vue';
import SubNavigationComp from '@/components/SubNavigationComp.vue'
import ListComp from '@/components/ListComp.vue';

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(VueCookies);
Vue.use(VueI18n);
Vue.use(Vuetify);

describe('MessagesPage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('messages/getMessagesList');

    const authState = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, authState);
    store.commit('auth/SET_IS_LOADING', false, authState);
    store.commit('auth/SET_USER', userData, authState);
    store.commit('auth/SET_ROLES', ['superAdmin'], authState);

    const i18n = {
        locale: 'lv',
        fallbackLocale: 'lv',
        messages: require('../../src/locales/lv.json')
    };

    const wrapper = shallowMount(MessagesPage, {
        localVue,
        vuetify,
        store,
        router,
        i18n,
        mocks: { $t: (msg) => msg }
    })

    const messagesState = {
        messageList: null,
        unreadMessageCount: null,
        isLoading: true,
    };

    store.commit('messages/SET_IS_LOADING', false, messagesState);
    store.commit('messages/SET_MESSAGE_LIST', [
        { id: 1, option: 'APPLY_FOR_REPAIR', name: 'Test', email: 'test@test.com', phoneNumber: '123123', phoneCountry: 'EU', description: 'this is description', clientLanguage: 'lv', isRead: 0, isAnswered: 0, createdAt: '2022-03-19 17:00:48', editedAt: '2022-03-19 17:00:48' }
    ], messagesState);
    store.commit('messages/SET_UNREAD_MESSAGE_COUNT', 1, messagesState);

    it('check if MessagePage children exist', () => {
        expect(wrapper.findComponent(SubNavigationComp).exists()).toBe(true)
        expect(wrapper.findComponent(ListComp).exists()).toBe(true)
    })
})
