import Vue from 'vue';
import Vuetify from 'vuetify';
import VueI18n from 'vue-i18n';

import AlertPopup from '@/components/popups/AlertPopup.vue';

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(VueI18n);
Vue.use(Vuetify);

describe('AlertPopup.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    const i18n = {
        locale: 'lv',
        fallbackLocale: 'lv',
        messages: require('../../src/locales/lv.json')
    };

    it('should match custom error message', () => {
        let errorMessage = 'error.pleaseTryAgainLater'
        const wrapper = shallowMount(AlertPopup, {
            localVue,
            vuetify,
            i18n,
            propsData: { error: errorMessage },
            mocks: { $t: (msg) => msg }
        })

        const alertMessage = wrapper.find('#alertMessage')

        expect(alertMessage.text()).toBe(errorMessage)
    })
})
