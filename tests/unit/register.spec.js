import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';
import VueI18n from 'vue-i18n';

import RegisterPage from '@/pages/RegisterPage.vue';
import SubNavigationComp from '@/components/SubNavigationComp.vue';
import AlertPopup from '@/components/popups/AlertPopup';

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(VueI18n);
Vue.use(Vuetify)

describe('RegisterPage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('message/setData');
    store.dispatch('messages/getMessagesList');
    store.dispatch('auth/me');

    const state = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, state);
    store.commit('auth/SET_IS_LOADING', false, state);
    store.commit('auth/SET_USER', userData, state);
    store.commit('auth/SET_ROLES', ['superAdmin'], state);

    const i18n = {
        locale: 'lv',
        fallbackLocale: 'lv',
        messages: require('../../src/locales/lv.json')
    };

    const wrapper = shallowMount(RegisterPage, {
        localVue,
        vuetify,
        store,
        router,
        i18n,
        mocks: { $t: (msg) => msg }
    })

    const newUserData = {
        username: 'testuser01',
        email: 'testuser01@sf.co',
        password: 'testpassword',
        password_confirmation: 'testpassword',
        language: 'en',
        isConfirmed: true,
        roles: [ 'addNewUser', 'seeStatistics' ],
        recaptcha: '',
    }

    wrapper.setData({ form: newUserData })

    it('check if register user values are correct', () => {
        expect(wrapper.vm.form.username).toEqual(newUserData.username);
        expect(wrapper.vm.form.email).toEqual(newUserData.email);
        expect(wrapper.vm.form.password).toEqual(newUserData.password);
        expect(wrapper.vm.form.password).toEqual(newUserData.password_confirmation);
        expect(wrapper.vm.form.language).toEqual(newUserData.language);
        expect(wrapper.vm.form.isConfirmed).toEqual(newUserData.isConfirmed);
        expect(wrapper.vm.form.roles).toEqual(newUserData.roles);
    })

    it('check if register user errors are correctly calculated', () => {
        expect(wrapper.vm.usernameErrors).toHaveLength(0);
        expect(wrapper.vm.emailErrors).toHaveLength(0);
        expect(wrapper.vm.passwordErrors).toHaveLength(0);
        expect(wrapper.vm.confirmPasswordErrors).toHaveLength(0);
        expect(wrapper.vm.languageErrors).toHaveLength(0);
        expect(wrapper.vm.isConfirmedErrors).toHaveLength(0);
        expect(wrapper.vm.roleErrors).toHaveLength(0);
    })

    it('check if children exist', () => {
        expect(wrapper.findComponent(SubNavigationComp).isVisible()).toBe(true);
        expect(wrapper.findComponent(AlertPopup).exists()).toBe(false);
    })
})
