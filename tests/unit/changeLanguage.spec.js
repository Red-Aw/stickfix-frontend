import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';

import ChangeLanguagePage from '@/pages/ChangeLanguagePage.vue';
import SubNavigationComp from '@/components/SubNavigationComp.vue'
import AlertPopup from '@/components/popups/AlertPopup'

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(Vuetify);

describe('ChangeLanguagePage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('message/setData');
    store.dispatch('messages/getMessagesList');
    store.dispatch('auth/me');

    const state = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, state);
    store.commit('auth/SET_IS_LOADING', false, state);
    store.commit('auth/SET_USER', userData, state);
    store.commit('auth/SET_ROLES', ['superAdmin'], state);

    const wrapper = shallowMount(ChangeLanguagePage, {
        localVue,
        vuetify,
        store,
        router,
        mocks: { $t: (msg) => msg }
    })

    let pageData = {
        id: userData.id,
        language: 'en',
        recaptcha: '',
    }

    wrapper.setData({ form: pageData })

    it('check if user language values are correct #1', () => {
        expect(wrapper.vm.form.language).toEqual(pageData.language);
        expect(wrapper.vm.languageErrors).toHaveLength(0);
    })

    pageData.language = null;

    wrapper.setData({ form: pageData })

    it('check if user language values are correct #2', () => {
        expect(wrapper.vm.form.language).toEqual(pageData.language);
        expect(wrapper.vm.languageErrors).toHaveLength(0);
    })

    it('check if service children exist', () => {
        expect(wrapper.findComponent(SubNavigationComp).exists()).toBe(true)
        expect(wrapper.findComponent(AlertPopup).exists()).toBe(false)
    })
})
