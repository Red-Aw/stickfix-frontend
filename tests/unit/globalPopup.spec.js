import Vue from 'vue';
import Vuetify from 'vuetify';

import GlobalPopup from '@/components/popups/GlobalPopup.vue';

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(Vuetify);

describe('GlobalPopup.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    it('should match custom error message', () => {
        let errorMessage = 'error.error'

        const wrapper = shallowMount(GlobalPopup, {
            localVue,
            vuetify,
            propsData: { message: errorMessage, isNotification: false },
            mocks: { $t: (msg) => msg }
        })

        const globalPopup = wrapper.find('#globalMessage')
        expect(globalPopup.text()).toBe(errorMessage)
    })
})
