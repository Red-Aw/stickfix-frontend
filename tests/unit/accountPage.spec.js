import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';

import AccountPage from '@/pages/AccountPage.vue';
import SubNavigationComp from '@/components/SubNavigationComp.vue'

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(Vuetify);

describe('AccountPage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('message/setData');
    store.dispatch('messages/getMessagesList');

    const state = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.com',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, state);
    store.commit('auth/SET_IS_LOADING', false, state);
    store.commit('auth/SET_USER', userData, state);
    store.commit('auth/SET_ROLES', ['superAdmin'], state);

    const wrapper = shallowMount(AccountPage, {
        localVue,
        vuetify,
        store,
        router,
        mocks: { $t: (msg) => msg }
    })

    it('check if user is authenticated', () => {
        expect(wrapper.vm.authenticated).toEqual(true);
    })

    it('check if user account values are displayed', () => {
        expect(wrapper.find('#username').text()).toBe(userData.username)
        expect(wrapper.find('#userEmail').text()).toBe(userData.email)
        expect(wrapper.find('#userLanguage').text()).toBe(userData.language)
        expect(wrapper.find('#userConfirmed').text()).toBe(userData.confirmed ? 'allowed' : 'denied' )
    })

    it('check if children exist', () => {
        expect(wrapper.findComponent(SubNavigationComp).exists()).toBe(true)
    })
})
