import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';

import AddOfferPage from '@/pages/AddOfferPage.vue';
import SubNavigationComp from '@/components/SubNavigationComp.vue'
import AlertPopup from '@/components/popups/AlertPopup'

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(Vuetify);

describe('AddOfferPage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('message/setData');
    store.dispatch('messages/getMessagesList');
    store.dispatch('auth/me');

    const state = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, state);
    store.commit('auth/SET_IS_LOADING', false, state);
    store.commit('auth/SET_USER', userData, state);
    store.commit('auth/SET_ROLES', ['superAdmin'], state);

    const wrapper = shallowMount(AddOfferPage, {
        localVue,
        vuetify,
        store,
        router,
        mocks: { $t: (msg) => msg }
    })

    const offerData = {
        title: 'test offer',
        description: 'this is test offer',
        isActive: false,
        newImages: [],
        recaptcha: '',
    }

    wrapper.setData({ form: offerData })

    it('check if add offer values are correct', () => {
        expect(wrapper.vm.form.title).toEqual(offerData.title);
        expect(wrapper.vm.form.description).toEqual(offerData.description);
        expect(wrapper.vm.form.isActive).toEqual(offerData.isActive);
    })

    it('check if add offer errors are correctly calculated', () => {
        expect(wrapper.vm.titleErrors).toHaveLength(0);
        expect(wrapper.vm.descriptionErrors).toHaveLength(0);
        expect(wrapper.vm.isActiveErrors).toHaveLength(0);
    })

    it('check if children exist', () => {
        expect(wrapper.findComponent(SubNavigationComp).exists()).toBe(true)
        expect(wrapper.findComponent(AlertPopup).exists()).toBe(false)
    })
})
