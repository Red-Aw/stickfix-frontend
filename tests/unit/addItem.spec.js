import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';

import AddItemPage from '@/pages/AddItemPage.vue';
import SubNavigationComp from '@/components/SubNavigationComp.vue'
import AlertPopup from '@/components/popups/AlertPopup'

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(Vuetify);

describe('AddItemPage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('message/setData');
    store.dispatch('messages/getMessagesList');
    store.dispatch('auth/me');

    const state = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, state);
    store.commit('auth/SET_IS_LOADING', false, state);
    store.commit('auth/SET_USER', userData, state);
    store.commit('auth/SET_ROLES', ['superAdmin'], state);

    const wrapper = shallowMount(AddItemPage, {
        localVue,
        vuetify,
        store,
        router,
        mocks: { $t: (msg) => msg }
    })

    const itemData = {
        title: 'test item',
        description: '',
        brand: 'BAUER',
        price: 111.00,
        state: 'NEW',
        category: 'SKATES',
        size: '',
        stickFlex: '',
        bladeCurve: '',
        bladeSide: 'LEFT',
        stickSize: '',
        skateLength: '3.5',
        skateWidth: 'D',
        isActive: true,
        newImages: [],
        recaptcha: '',
    }

    wrapper.setData({ form: itemData })

    it('check if add item values are correct', () => {
        expect(wrapper.vm.form.title).toEqual(itemData.title);
        expect(wrapper.vm.form.description).toEqual(itemData.description);
        expect(wrapper.vm.form.brand).toEqual(itemData.brand);
        expect(wrapper.vm.form.price).toEqual(itemData.price);
        expect(wrapper.vm.form.state).toEqual(itemData.state);
        expect(wrapper.vm.form.category).toEqual(itemData.category);
        expect(wrapper.vm.form.skateLength).toEqual(itemData.skateLength);
        expect(wrapper.vm.form.skateWidth).toEqual(itemData.skateWidth);
        expect(wrapper.vm.form.stickSize).toEqual(itemData.stickSize);
        expect(wrapper.vm.form.bladeCurve).toEqual(itemData.bladeCurve);
        expect(wrapper.vm.form.bladeSide).toEqual(itemData.bladeSide);
        expect(wrapper.vm.form.stickFlex).toEqual(itemData.stickFlex);
        expect(wrapper.vm.form.size).toEqual(itemData.size);
        expect(wrapper.vm.form.isActive).toEqual(itemData.isActive);
    })

    it('check if add item errors are correctly calculated', () => {
        expect(wrapper.vm.titleErrors).toHaveLength(0);
        expect(wrapper.vm.descriptionErrors).toHaveLength(0);
        expect(wrapper.vm.brandErrors).toHaveLength(0);
        expect(wrapper.vm.priceErrors).toHaveLength(0);
        expect(wrapper.vm.stateErrors).toHaveLength(0);
        expect(wrapper.vm.skateLengthErrors).toHaveLength(0);
        expect(wrapper.vm.skateWidthErrors).toHaveLength(0);
        expect(wrapper.vm.stickSizeErrors).toHaveLength(0);
        expect(wrapper.vm.bladeCurveErrors).toHaveLength(0);
        expect(wrapper.vm.bladeSideErrors).toHaveLength(0);
        expect(wrapper.vm.stickFlexErrors).toHaveLength(0);
        expect(wrapper.vm.sizeErrors).toHaveLength(0);
        expect(wrapper.vm.isActiveErrors).toHaveLength(0);
    })

    it('check if children exist', () => {
        expect(wrapper.findComponent(SubNavigationComp).exists()).toBe(true)
        expect(wrapper.findComponent(AlertPopup).exists()).toBe(false)
    })
})
