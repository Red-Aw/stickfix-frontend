import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';
import VueCookies from 'vue-cookies';
import VueI18n from 'vue-i18n';

import App from '@/App.vue';
import GlobalPopup from '@/components/popups/GlobalPopup.vue'
import NavComp from '@/components/NavComp.vue'
import FooterComp from '@/components/FooterComp.vue'

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(VueCookies);
Vue.use(VueI18n);
Vue.use(Vuetify);

describe('App.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('messages/getMessagesList');

    const authState = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, authState);
    store.commit('auth/SET_IS_LOADING', false, authState);
    store.commit('auth/SET_USER', userData, authState);
    store.commit('auth/SET_ROLES', ['superAdmin'], authState);

    const messageState = {
        message: null,
        visible: false,
        type: null,
    };

    store.commit('message/SET_MESSAGE', 'error.error', messageState);
    store.commit('message/SET_VISIBLE', true, messageState);
    store.commit('message/SET_TYPE', 'error', messageState);

    const i18n = {
        locale: 'lv',
        fallbackLocale: 'lv',
        messages: require('../../src/locales/lv.json')
    };

    const wrapper = shallowMount(App, {
        localVue,
        vuetify,
        store,
        router,
        i18n,
        mocks: { $t: (msg) => msg }
    })

    it('check if children exist', () => {
        expect(wrapper.findComponent(GlobalPopup).exists()).toBe(false)

        expect(wrapper.findComponent(NavComp).exists()).toBe(true)

        expect(wrapper.findComponent(FooterComp).exists()).toBe(true)
    })

    store.commit('message/SET_VISIBLE', false, messageState);

    it('check if optional children exist', () => {
        expect(wrapper.findComponent(GlobalPopup).exists()).toBe(false)
    })
})
