import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';

import LoginPage from '@/pages/LoginPage.vue';
import AlertPopup from '@/components/popups/AlertPopup'

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(Vuetify);

describe('LoginPage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('message/setData');
    store.dispatch('messages/getMessagesList');
    store.dispatch('auth/me');

    const state = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, state);
    store.commit('auth/SET_IS_LOADING', false, state);
    store.commit('auth/SET_USER', userData, state);
    store.commit('auth/SET_ROLES', ['superAdmin'], state);

    const wrapper = shallowMount(LoginPage, {
        localVue,
        vuetify,
        store,
        router,
        mocks: { $t: (msg) => msg }
    })

    const loginData = {
        email: userData.email,
        password: 'testpassword',
        recaptcha: '',
    }

    wrapper.setData({ form: loginData })

    it('check if login values are correct', () => {
        expect(wrapper.vm.form.email).toEqual(loginData.email);
        expect(wrapper.vm.form.password).toEqual(loginData.password);
    })

    it('check if add item errors are correctly calculated', () => {
        expect(wrapper.vm.emailErrors).toHaveLength(0);
        expect(wrapper.vm.passwordErrors).toHaveLength(0);
    })

    it('check if children exist', () => {
        expect(wrapper.findComponent(AlertPopup).exists()).toBe(false)
    })
})
