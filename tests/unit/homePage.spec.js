import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';
import VueCookies from 'vue-cookies';
import VueI18n from 'vue-i18n';

import HomePage from '@/pages/HomePage.vue';
import CarouselComp from '@/components/CarouselComp.vue';
import ListComp from '@/components/ListComp.vue';
import CardComp from '@/components/CardComp.vue';
import StepperComp from '@/components/CardComp.vue';

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(VueCookies);
Vue.use(VueI18n);
Vue.use(Vuetify);

describe('HomePage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('messages/getMessagesList');

    const authState = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, authState);
    store.commit('auth/SET_IS_LOADING', false, authState);
    store.commit('auth/SET_USER', userData, authState);
    store.commit('auth/SET_ROLES', ['superAdmin'], authState);

    const i18n = {
        locale: 'lv',
        fallbackLocale: 'lv',
        messages: require('../../src/locales/lv.json')
    };

    const wrapper = shallowMount(HomePage, {
        localVue,
        vuetify,
        store,
        router,
        i18n,
        mocks: { $t: (msg) => msg }
    })

    const instagramState = {
        posts: null,
        isLoading: true,
    };

    store.commit('instagram/SET_IS_LOADING', false, instagramState);
    store.commit('instagram/SET_POSTS', [], instagramState);

    const servicesState = {
        servicesList: null,
        isLoading: true,
    };

    store.commit('services/SET_IS_LOADING', false, servicesState);
    store.commit('services/SET_SERVICES_LIST', [
        { id: 1, serviceType: 'OFFER', title: 'this is title', note: 'this is note', priceInMinorUnit: 1000, isActive: 1, createdAt: '2022-03-19 17:00:48', editedAt: '2022-03-19 17:00:48' }
    ], servicesState);

    const offersState = {
        offersList: null,
        isLoading: true,
    };

    store.commit('offers/SET_IS_LOADING', false, offersState);
    store.commit('offers/SET_OFFERS_LIST', [
        { id: 1, title: 'this is title', description: 'this is description', isActive: 1, createdAt: '2022-03-19 17:00:48', editedAt: '2022-03-19 17:00:48' }
    ], offersState);

    it('check if HomePage children exist', () => {
        expect(wrapper.findComponent(CarouselComp).exists()).toBe(true)
        expect(wrapper.findComponent(ListComp).exists()).toBe(true)
        expect(wrapper.findComponent(CardComp).exists()).toBe(true)
        expect(wrapper.findComponent(StepperComp).exists()).toBe(true)
    })
})
