import Vue from 'vue';
import Vuetify from 'vuetify';
import store from '../../src/store/vuex';
import router from '../../src/router';

import AddServicePage from '@/pages/AddServicePage.vue';
import SubNavigationComp from '@/components/SubNavigationComp.vue'
import AlertPopup from '@/components/popups/AlertPopup'

import { createLocalVue, shallowMount } from '@vue/test-utils';

const localVue = createLocalVue();

Vue.use(Vuetify);

describe('AddServicePage.vue', () => {
    let vuetify;

    beforeEach(() => {
        vuetify = new Vuetify()
    })

    store.dispatch('items/getItemsList');
    store.dispatch('services/getServicesList');
    store.dispatch('offers/getOffersList');
    store.dispatch('enums/getAllEnums');
    store.dispatch('message/setData');
    store.dispatch('messages/getMessagesList');
    store.dispatch('auth/me');

    const state = {
        authenticated: false,
        user: null,
        roles: null,
        allRoles: null,
        isLoading: true,
    };

    const userData = {
        id: 1,
        username: 'admin',
        email: 'admin@admin.co',
        confirmed: true,
        language: 'lv',
        createdAt: '2022-03-17 22:21:33',
        editedAt: '2022-03-17 22:21:33',
        passEditedAt: '2022-03-17 22:21:33',
    };

    store.commit('auth/SET_AUTHENTICATED', true, state);
    store.commit('auth/SET_IS_LOADING', false, state);
    store.commit('auth/SET_USER', userData, state);
    store.commit('auth/SET_ROLES', ['superAdmin'], state);

    const wrapper = shallowMount(AddServicePage, {
        localVue,
        vuetify,
        store,
        router,
        mocks: { $t: (msg) => msg }
    })

    const serviceData = {
        serviceType: 'OFFER',
        title: 'test service',
        note: 'this is test service',
        price: 99.99,
        isActive: true,
        newImages: [],
        recaptcha: '',
    }

    wrapper.setData({ form: serviceData })

    it('check if add service values are correct', () => {
        expect(wrapper.vm.form.serviceType).toEqual(serviceData.serviceType);
        expect(wrapper.vm.form.title).toEqual(serviceData.title);
        expect(wrapper.vm.form.note).toEqual(serviceData.note);
        expect(wrapper.vm.form.price).toEqual(serviceData.price);
        expect(wrapper.vm.form.isActive).toEqual(serviceData.isActive);
    })

    it('check if add service errors are correctly calculated', () => {
        expect(wrapper.vm.serviceTypeErrors).toHaveLength(0);
        expect(wrapper.vm.titleErrors).toHaveLength(0);
        expect(wrapper.vm.noteErrors).toHaveLength(0);
        expect(wrapper.vm.priceErrors).toHaveLength(0);
        expect(wrapper.vm.isActiveErrors).toHaveLength(0);
    })

    it('check if service children exist', () => {
        expect(wrapper.findComponent(SubNavigationComp).exists()).toBe(true)
        expect(wrapper.findComponent(AlertPopup).exists()).toBe(false)
    })
})
