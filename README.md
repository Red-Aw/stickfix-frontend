StickFix Frontend. Built with VueJs 2, Vuetify 2. Built by Nethall

To deploy:
1. add .env file. NODE_ENV=production, add correct URLs, reCaptcha keys and Google Maps key.
2. run: npm install
3. run: npm run build. node_modules map not needed in prod


---------------------------------------------------------------

Commands:


Project setup:
* npm install

<br />

Compiles and minifies for production:
* npm run build

<br />

Compiles and hot-reloads for development:
* npm run serve <br />
* npm run watch

<br />

Testing:

* npm run test:unit
